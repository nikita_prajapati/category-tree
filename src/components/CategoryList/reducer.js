import { GET_CATEGORY_LIST_FAILURE, GET_CATEGORY_LIST_PENDING, GET_CATEGORY_LIST_SUCCESS } from "../../redux/actionTypes";
export default function categoryList(state = {
    categoryList: {
        loading: false,
        success: false,
        error: false,
        data: []
    }
}, action) {
    switch (action.type) {

        case GET_CATEGORY_LIST_PENDING:
            return Object.assign({}, state,
                {
                    categoryList: { data: [], loading: true, success: false, error: false }
                })
        case GET_CATEGORY_LIST_SUCCESS:
            return Object.assign({}, state,
                {
                    categoryList: { data: action.categoryList, loading: false, success: true, error: false }
                })
        case GET_CATEGORY_LIST_FAILURE:
            return Object.assign({}, state,
                {
                    categoryList: { data: [], loading: false, success: false, error: true }
                })
        default:
            return state;
    }
}