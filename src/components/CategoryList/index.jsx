import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Category from "./Category";
import * as categoryActions from "./actions";
import "./style.css";

const CategoryList = (props) => {
  return (
    <div>
      {props.categoryList.loading && <div>Loading...</div>}
      {props.dataList.length > 0 && (
        <div className={`category-list ${props.title} `}>
          <h1>{props.title}</h1>
          {props.dataList.map((category, index) => {
            return (
              <Category
                category={category}
                isExpandAllCategories={props.isExpandAllCategories}
                setIsExpandAllCategories={props.setIsExpandAllCategories}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    categoryList: state.categoryListReducer.categoryList,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);
