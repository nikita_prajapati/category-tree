import { getLocalStorage } from "../../utilities/storage";
function getCategoryList() {

    let categories = JSON.parse(getLocalStorage("categories"));

    return new Promise((resolve, reject) => { resolve(categories || []) })
}

const apiService = {
    getCategoryList
}

export default apiService;