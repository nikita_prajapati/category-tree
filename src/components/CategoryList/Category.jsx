import React, { useState } from "react";
import CategoryList from "./index";
import AddEditCategory from "../AddEditCategory";
import DeleteCategory from "../DeleteCategory";
import "./style.css";

const Category = (props) => {
  const [showSubCategory, setShowCategory] = useState({ show: false });
  const [showMenuItems, setShowMenuItems] = useState({ show: false });
  const [showPopup, setShowPopup] = useState({ show: false });
  const [showDeleteCategory, setShowDeleteCategory] = useState({ show: false });

  const onPlusClick = () => {
    let subCategoryData = { ...showSubCategory };
    subCategoryData.show = !showSubCategory.show;
    subCategoryData.id = props.category.id;
    setShowCategory(subCategoryData);
    props.setIsExpandAllCategories(false);
  };
  const onMinusClick = () => {
    let subCategoryData = { ...showSubCategory };
    subCategoryData.show = false;
    setShowCategory(subCategoryData);
    props.setIsExpandAllCategories(false);
  };

  const onCategoryClick = () => {
    let menuItems = { ...showMenuItems };
    menuItems.show = !menuItems.show;
    menuItems.id = props.category.id;
    setShowMenuItems(menuItems);
  };

  const onAddEditCategoryClick = (action) => {
    let popupData = { ...showPopup };
    popupData.show = true;
    popupData.action = action;
    if (action === "Edit Category") {
      popupData.id = props.category.id;
      popupData.name = props.category.name;
      popupData.parentId = props.category.parentId;
    }
    if (action === "Add Category") {
      popupData.parentId = props.category.id;
    }
    setShowPopup(popupData);
  };
  const closePopup = () => {
    let popupData = { ...showPopup };
    popupData.show = false;
    popupData.action = "";
    setShowPopup(popupData);
  };
  const deleteCategory = () => {
    let deleteCategoryData = { ...showDeleteCategory };
    deleteCategoryData.show = true;
    deleteCategoryData.id = props.category.id;
    setShowDeleteCategory(deleteCategoryData);
  };
  const onDeleteCategoryCancelClick = () => {
    let deleteCategoryData = { ...showDeleteCategory };
    deleteCategoryData.show = false;
    setShowDeleteCategory(deleteCategoryData);
  };
  return (
    <div className="category" id={props.index}>
      <span className="category-span" onClick={onCategoryClick}>
        {props.category.name}
      </span>
      {props.isExpandAllCategories || showSubCategory.show ? (
        <button onClick={onMinusClick}>
          <i class="fa fa-minus"></i>
        </button>
      ) : (
        <button onClick={onPlusClick}>
          <i class="fa fa-plus"></i>
        </button>
      )}
      {showMenuItems.show && showMenuItems.id === props.category.id && (
        <div className="alert alert-primary rate">
          <button
            className="add-category"
            onClick={() => {
              onAddEditCategoryClick("Add Category");
            }}
          >
            Add Category
          </button>
          <button
            className="edit-category"
            onClick={() => {
              onAddEditCategoryClick("Edit Category");
            }}
          >
            Edit Category
          </button>
          <button className="delete-category" onClick={deleteCategory}>
            Delete Category
          </button>
        </div>
      )}
      {((showSubCategory.show &&
        props.category.id === showSubCategory.id &&
        props.category.children.length > 0) ||
        props.isExpandAllCategories) && (
        <CategoryList
          dataList={props.category.children}
          title={"sub category"}
          isExpandAllCategories={props.isExpandAllCategories}
          setIsExpandAllCategories={props.setIsExpandAllCategories}
        />
      )}
      {showPopup.show && (
        <AddEditCategory
          handleClose={closePopup}
          parentId={showPopup.parentId}
          title={showPopup.action}
          id={showPopup.id}
          name={showPopup.name}
        />
      )}
      {showDeleteCategory.show && (
        <DeleteCategory
          id={showDeleteCategory.id}
          handleClose={onDeleteCategoryCancelClick}
        />
      )}
    </div>
  );
};

export default Category;
