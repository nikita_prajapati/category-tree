import { GET_CATEGORY_LIST_FAILURE, GET_CATEGORY_LIST_PENDING, GET_CATEGORY_LIST_SUCCESS } from "../../redux/actionTypes";
import apiService from "./apiService";

export function getCategoryListPending() {
    return {
        type: GET_CATEGORY_LIST_PENDING
    }
}
export function getCategoryListSuccess(categoryList) {
    return {
        type: GET_CATEGORY_LIST_SUCCESS,
        categoryList
    }
}

export function getCategoryListFailure() {
    return {
        type: GET_CATEGORY_LIST_FAILURE
    }
}
export function getCategoryList() {
    return dispatch => {
        dispatch(getCategoryListPending());
        apiService.getCategoryList().then(response => {
            dispatch(getCategoryListSuccess(response));
        }).catch(error => {
            dispatch(getCategoryListFailure())
        })
    }
}