import React from "react";
import Modal from "react-bootstrap/Modal";

const ModalComponent = ({ title, modalBody, modalFooter, handleClose }) => {
  return (
    <div>
      <Modal show={true} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{modalBody}</Modal.Body>
        <Modal.Footer>{modalFooter}</Modal.Footer>
      </Modal>
    </div>
  );
};

export default ModalComponent;
