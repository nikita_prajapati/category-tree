import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AddEditCategory from "../AddEditCategory/index";
import CategoryList from "../CategoryList/index";
import * as categoryActions from "../CategoryList/actions";
const MainContainer = (props) => {
  const [isAddCategoryModalOpen, setIsAddCategoryModalOpen] = useState(false);
  const [isExpandAllCategories, setIsExpandAllCategories] = useState(false);
  const getCategoryList = () => {
    props.categoryActions.getCategoryList();
  };
  useEffect(getCategoryList, [
    props.createCategory.success,
    props.editCategory.success,
    props.deleteCategory.success,
  ]);

  const openAddCategoryModal = () => {
    setIsAddCategoryModalOpen(true);
  };

  const CloseAddEditCategoryModal = () => {
    setIsAddCategoryModalOpen(false);
  };

  const getTreeStructure = (data) => {
    console.log("data called::", data);
    let flatData = data || [];
    let treeData = [];

    flatData = flatData.map((data) => {
      data.children = [];
      return data;
    });
    for (let i = 0; i < flatData.length; i++) {
      let arrElem = flatData[i];
      if (arrElem.parentId) {
        let parentData = flatData.filter((data) => {
          return data.id === arrElem.parentId;
        });
        if (parentData.length > 0) {
          parentData[0]["children"].push(arrElem);
        } else {
          treeData.push(arrElem);
        }
      } else {
        treeData.push(arrElem);
      }
    }
    return treeData;
  };

  const expandAllCategories = () => {
    setIsExpandAllCategories(!isExpandAllCategories);
  };
  return (
    <div>
      <button
        className="btn btn-primary add-category"
        onClick={openAddCategoryModal}
      >
        Add Category
      </button>
      {isAddCategoryModalOpen && (
        <AddEditCategory
          handleClose={CloseAddEditCategoryModal}
          title={"Add Category"}
        />
      )}
      <div>
        <button onClick={expandAllCategories}>Expand All</button>
        <CategoryList
          dataList={getTreeStructure(props.categoryList.data)}
          title={"category"}
          isExpandAllCategories={isExpandAllCategories}
          setIsExpandAllCategories={setIsExpandAllCategories}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    createCategory: state.addEditCategoryReducer.createCategory,
    editCategory: state.addEditCategoryReducer.editCategory,
    categoryList: state.categoryListReducer.categoryList,
    deleteCategory: state.deleteCategoryReducer.deleteCategory,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
