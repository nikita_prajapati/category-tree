import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ModalComponent from "../Modal";
import * as categoryActions from "./actions";

const DeleteCategory = ({ handleClose, id, categoryActions }) => {
  const onOkClick = () => {
    categoryActions.deleteCategory(id);
    handleClose();
  };
  const renderModalBody = () => {
    return (
      <div>
        This will delete all sub-categories associated with it. Are you sure you
        want to delete this category?
      </div>
    );
  };
  const renderModalFooter = () => {
    return (
      <>
        <button className="btn btn-primary" onClick={onOkClick}>
          Ok
        </button>
        <button className="btn btn-secondary" onClick={handleClose}>
          Cancel
        </button>
      </>
    );
  };

  return (
    <div>
      <ModalComponent
        title={"Delete Category"}
        modalBody={renderModalBody()}
        modalFooter={renderModalFooter()}
        handleClose={handleClose}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch) => {
  return {
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteCategory);
