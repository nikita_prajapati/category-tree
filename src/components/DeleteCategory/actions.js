import { DELETE_CATEGORY_FAILURE, DELETE_CATEGORY_PENDING, DELETE_CATEGORY_SUCCESS } from "../../redux/actionTypes";
import apiService from "./apiService";

export function deleteCategoryPending() {
    return {
        type: DELETE_CATEGORY_PENDING
    }
}
export function deleteCategorySuccess() {
    return {
        type: DELETE_CATEGORY_SUCCESS,
    }
}

export function deleteCategoryFailure() {
    return {
        type: DELETE_CATEGORY_FAILURE
    }
}
export function deleteCategory(id) {
    return dispatch => {
        dispatch(deleteCategoryPending());
        apiService.deleteCategory(id).then(response => {
            dispatch(deleteCategorySuccess());
        }).catch(error => {
            dispatch(deleteCategoryFailure())
        })
    }
}