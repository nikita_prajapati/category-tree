import { getLocalStorage, setLocalStorage } from "../../utilities/storage";
function deleteCategory(id) {

    let categories = JSON.parse(getLocalStorage("categories"));

    let newCategories = categories.filter((category => {
        return category.id !== id;
    }));
    newCategories = newCategories.filter((category => {
        return category.parentId !== id;
    }))

    console.log("in delete category::", newCategories)
    setLocalStorage("categories", JSON.stringify(newCategories))
    return new Promise((resolve, reject) => { resolve("deleted success") })
}

const apiService = {
    deleteCategory
}

export default apiService;