import { DELETE_CATEGORY_FAILURE, DELETE_CATEGORY_PENDING, DELETE_CATEGORY_SUCCESS } from "../../redux/actionTypes";

export default function deleteCategory(state = {
    deleteCategory: {
        loading: false,
        success: false,
        error: false,
    }
}, action) {
    switch (action.type) {

        case DELETE_CATEGORY_PENDING:
            return Object.assign({}, state,
                {
                    deleteCategory: { loading: true, success: false, error: false }
                })
        case DELETE_CATEGORY_SUCCESS:
            return Object.assign({}, state,
                {
                    deleteCategory: { loading: false, success: true, error: false }
                })
        case DELETE_CATEGORY_FAILURE:
            return Object.assign({}, state,
                {
                    deleteCategory: { loading: false, success: false, error: true }
                })
        default:
            return state;
    }
}