import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ModalComponent from "../Modal";
import * as categoryActions from "./actions";

const AddEditCategory = ({
  handleClose,
  categoryActions,
  parentId,
  title,
  id,
  name,
}) => {
  const [categoryName, setCategoryName] = useState("");
  useEffect(() => {
    if (name) setCategoryName(name);
  }, [name]);

  const onSaveClick = () => {
    if (!categoryName) {
      return;
    }
    let payload = {};

    payload.name = categoryName;
    payload.totalSubCategories = 0;
    if (parentId) payload.parentId = parentId;
    if (id) {
      payload.id = id;
      categoryActions.editCategory(payload);
    } else {
      categoryActions.createCategory(payload);
    }
    handleClose();
  };

  const renderModalBody = () => {
    return (
      <input
        type="text"
        name="addEditCategory"
        value={categoryName}
        onChange={(event) => {
          setCategoryName(event.target.value);
        }}
      />
    );
  };
  const renderModalFooter = () => {
    return (
      <>
        <button className="btn btn-primary" onClick={onSaveClick}>
          Save
        </button>
        <button className="btn btn-secondary" onClick={handleClose}>
          Cancel
        </button>
      </>
    );
  };
  return (
    <div>
      <ModalComponent
        title={title || ""}
        modalBody={renderModalBody()}
        modalFooter={renderModalFooter()}
        handleClose={handleClose}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch) => {
  return {
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEditCategory);
