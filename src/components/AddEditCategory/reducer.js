import { CREATE_CATEGORY_PENDING, CREATE_CATEGORY_SUCCESS, CREATE_CATEGORY_FAILURE, EDIT_CATEGORY_PENDING, EDIT_CATEGORY_SUCCESS, EDIT_CATEGORY_FAILURE } from "../../redux/actionTypes"

export default function addEditCategory(state = {
    createCategory: {
        loading: false,
        success: false,
        error: false
    },
    editCategory: {
        loading: false,
        success: false,
        error: false
    }
}, action) {
    switch (action.type) {

        case CREATE_CATEGORY_PENDING:
            return Object.assign({}, state,
                {
                    createCategory: { loading: true, success: false, error: false }
                })
        case CREATE_CATEGORY_SUCCESS:
            return Object.assign({}, state,
                {
                    createCategory: { loading: false, success: true, error: false }
                })
        case CREATE_CATEGORY_FAILURE:
            return Object.assign({}, state,
                {
                    createCategory: { loading: false, success: false, error: true }
                })
        case EDIT_CATEGORY_PENDING:
            return Object.assign({}, state,
                {
                    editCategory: { loading: true, success: false, error: false }
                })
        case EDIT_CATEGORY_SUCCESS:
            return Object.assign({}, state,
                {
                    editCategory: { loading: false, success: true, error: false }
                })
        case EDIT_CATEGORY_FAILURE:
            return Object.assign({}, state,
                {
                    editCategory: { loading: false, success: false, error: true }
                })
        default:
            return state;
    }
}