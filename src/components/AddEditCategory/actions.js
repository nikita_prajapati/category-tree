import { CREATE_CATEGORY_PENDING, CREATE_CATEGORY_SUCCESS, CREATE_CATEGORY_FAILURE,EDIT_CATEGORY_PENDING, EDIT_CATEGORY_SUCCESS, EDIT_CATEGORY_FAILURE } from "../../redux/actionTypes"
import apiService from './apiService';

export function createCategoryPending() {
    return {
        type: CREATE_CATEGORY_PENDING
    }
}

export function createCategorySuccess() {
    return {
        type: CREATE_CATEGORY_SUCCESS
    }
}

export function createCategoryFailure() {
    return {
        type: CREATE_CATEGORY_FAILURE
    }
}

export function createCategory(payload) {
    return dispatch => {
        dispatch(createCategoryPending());
        apiService.createCategory(payload).then(response => {
            dispatch(createCategorySuccess());
        }).catch(error => {
            dispatch(createCategoryFailure());
        })
    }
}

export function editCategoryPending() {
    return {
        type: EDIT_CATEGORY_PENDING
    }
}

export function editCategorySuccess() {
    return {
        type: EDIT_CATEGORY_SUCCESS
    }
}

export function editCategoryFailure() {
    return {
        type: EDIT_CATEGORY_FAILURE
    }
}

export function editCategory(payload) {
    return dispatch => {
        dispatch(editCategoryPending());
        apiService.editCategory(payload).then(response => {
            dispatch(editCategorySuccess());
        }).catch(error => {
            dispatch(editCategoryFailure());
        })
    }
}