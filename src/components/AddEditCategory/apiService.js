import { getLocalStorage, setLocalStorage } from "../../utilities/storage";


function createCategory(payload) {
    let categories = JSON.parse(getLocalStorage("categories"));
    console.log("categories::", categories)
    if (!categories) {
        categories = [];
    }
    payload.id = categories.length + 1;
    categories.push(payload);
    setLocalStorage("categories", JSON.stringify(categories))
    return new Promise((resolve, reject) => { resolve(payload) })
}
function editCategory(payload) {
    let categories = JSON.parse(getLocalStorage("categories"));

    let index;
    for (let i = 0; i < categories.length; i++) {
        if (categories[i].id === payload.id) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        categories.splice(index, 1, payload);
    }
    console.log("payload::",payload)
    console.log("cate::",categories)
    setLocalStorage("categories", JSON.stringify(categories))
    return new Promise((resolve, reject) => { resolve(payload) })
}

const apiService = {
    createCategory,
    editCategory
}

export default apiService;