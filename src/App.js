import React from 'react';
import { Provider } from "react-redux";
import configureStore from "./redux/store";
import './App.css';
import MainContainer from './components/Main';

function App() {
  return (
    <Provider store={configureStore()}>
      <div className="App">
        <MainContainer />
      </div>
    </Provider>
  );
}

export default App;
