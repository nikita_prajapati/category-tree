import { combineReducers } from "redux";
import addEditCategoryReducer from "../components/AddEditCategory/reducer";
import categoryListReducer from "../components/CategoryList/reducer";
import deleteCategoryReducer from "../components/DeleteCategory/reducer";

const rootReducer = combineReducers({
    addEditCategoryReducer: addEditCategoryReducer,
    categoryListReducer: categoryListReducer,
    deleteCategoryReducer: deleteCategoryReducer
});

export default rootReducer;